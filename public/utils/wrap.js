export function wrap(value, count) {
  return value < 0
    ? value + Math.ceil(Math.abs(value / count)) * count
    : value % count;
}
