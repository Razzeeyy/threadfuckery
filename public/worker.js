import { ByteArray2Wrap } from "./core/byte-array-2-wrap.js";
import { DoubleBuffer } from "./core/double-buffer.js";

var countNeighbours = (board, x, y) => {
  var neighbours = 0;
  for (var i = -1; i <= 1; ++i) {
    for (var j = -1; j <= 1; ++j) {
      if (i === 0 && j === 0) {
        continue; // skip yourself
      }
      if (board.get(x + i, y + j)) {
        neighbours += 1;
      }
    }
  }
  return neighbours;
};

var id = -1;
var world = null;
var fromX = 0;
var toX = 0;
var fromY = 0;
var toY = 0;

/*
0 0 330 0 235
1 329 659 0 235
2 329 659 235 235
3 0 330 235 235
*/

function assignComputationalBoundsFromId(id, workersTotal, ww, wh) {
  fromX = 0;
  toX = ww;
  // раздаём пространство на симуляцию горизонтальными полосками между воркерами
  var heightPerWorker = Math.ceil(wh / workersTotal);
  fromY = Math.min(id * heightPerWorker, wh);
  toY = Math.min(fromY + heightPerWorker, wh);
}

onmessage = ({ data }) => {
  if (typeof data.msg !== "string") {
    return;
  }

  switch (data.msg) {
    case "MSG_INIT": {
      id = data.id;
      var ww = data.worldWidth;
      var wh = data.worldHeight;
      assignComputationalBoundsFromId(id, data.workersTotal, ww, wh);
      world = new DoubleBuffer(
        new ByteArray2Wrap(ww, wh, data.activeBuffer),
        new ByteArray2Wrap(ww, wh, data.inactiveBuffer),
      );
      console.log(id, fromX, toX, fromY, toY);
      break;
    }
    case "MSG_STEP": {
      var current = world.active;
      var next = world.inactive;

      for (var x = fromX; x < toX; ++x) {
        for (var y = fromY; y < toY; ++y) {
          next.set(x, y, 0);
          var isAlive = current.get(x, y);
          var neighbours = countNeighbours(current, x, y);
          if (!isAlive) {
            neighbours === 3 && next.set(x, y, 1); // a miracle of birth!
          } else {
            neighbours < 2 || neighbours > 3
              ? next.set(x, y, 0)
              : next.set(x, y, 1);
          }
        }
      }
      world.swap();
      postMessage({ msg: "MSG_STEP_DONE", id });
      break;
    }
  }
};
