export class DoubleBuffer {
  constructor(active, inactive) {
    this._active = active;
    this._inactive = inactive;
  }

  get active() {
    return this._active;
  }

  get inactive() {
    return this._inactive;
  }

  swap() {
    var tmp = this._active;
    this._active = this._inactive;
    this._inactive = tmp;
  }
}
