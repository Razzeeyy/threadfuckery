import { wrap } from "../utils/wrap.js";

export class ByteArray2Wrap {
  constructor(width, height, buffer) {
    this._buffer = buffer ?? new SharedArrayBuffer(width * height);
    this._values = new Uint8Array(this._buffer);

    this._width = width;
    this._height = height;
  }

  clear() {
    this._values.fill(0);
  }

  get buffer() {
    return this._buffer;
  }

  get width() {
    return this._width;
  }

  get height() {
    return this._height;
  }

  set(x, y, value) {
    this._values[this.index(x, y)] = value;
  }

  get(x, y) {
    return this._values[this.index(x, y)];
  }

  isInBounds(x, y) {
    return x >= 0 && x < this._width && y >= 0 && y < this._height;
  }

  index(x, y) {
    x = wrap(x, this.width);
    y = wrap(y, this.height);

    return y * this.width + x;
  }
}
