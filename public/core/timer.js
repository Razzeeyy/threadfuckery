export function timer() {
  var lastTime = performance.now();
  return () => {
    var now = performance.now();
    var delta = now - lastTime;
    lastTime = now;
    return delta;
  };
}
