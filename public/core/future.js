export function createFuture() {
  var promiseResolve = () => {};
  var promiseReject = () => {};
  var promise = new Promise((resolve, reject) => {
    promiseResolve = resolve;
    promiseReject = reject;
  });
  promise.resolve = promiseResolve;
  promise.reject = promiseReject;
  return promise;
}
