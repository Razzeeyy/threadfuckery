import { DoubleBuffer } from "./core/double-buffer.js";
import { ByteArray2Wrap } from "./core/byte-array-2-wrap.js";
import { createFuture } from "./core/future.js";
import { timer } from "./core/timer.js";

var canvas = document.getElementById("display");
var ctx = canvas.getContext("2d");
canvas.width = window.innerWidth;
canvas.height = window.innerHeight;

var worldWidth = Math.ceil(canvas.width);
var worldHeight = Math.ceil(canvas.height);

var world = new DoubleBuffer(
  new ByteArray2Wrap(worldWidth, worldHeight),
  new ByteArray2Wrap(worldWidth, worldHeight)
);
for (var x = 0; x < worldWidth; ++x) {
  for (var y = 0; y < worldHeight; ++y) {
    world.active.set(x, y, +(Math.random() > 0.5));
  }
}

var WORKERS_TOTAL = navigator.hardwareConcurrency;
console.log("total workers", WORKERS_TOTAL);
var workers = [];
var futures = [];
for (var i = 0; i < WORKERS_TOTAL; ++i) {
  var worker = new Worker("./worker.js", { type: "module" });
  workers.push(worker);
  worker.postMessage({
    msg: "MSG_INIT",
    id: i,
    workersTotal: WORKERS_TOTAL,
    worldWidth,
    worldHeight,
    activeBuffer: world.active.buffer,
    inactiveBuffer: world.inactive.buffer,
  });
  worker.onmessage = ({ data }) => {
    if (!data?.msg === "MSG_STEP_DONE") {
      return;
    }
    var future = futures[data.id];
    future?.resolve();
  };
}

var timers = {
  update: timer(),
  render: timer(),
};

var stats = {
  update: 0,
  render: 0,
};

update();
async function update() {
  timers.update();
  for (var i = 0; i < WORKERS_TOTAL; ++i) {
    futures[i] = createFuture();
    workers[i].postMessage({
      msg: "MSG_STEP",
    });
  }
  await Promise.all(futures);
  world.swap();
  setTimeout(update, 0);
  stats.update = timers.update();
}

var imgData = ctx.createImageData(worldWidth, worldHeight);

render();
function render() {
  timers.render();
  ctx.clearRect(0, 0, canvas.width, canvas.height);
  ctx.fillStyle = "black";
  var pixels = imgData.data;
  for (var x = 0; x < worldWidth; ++x) {
    for (var y = 0; y < worldHeight; ++y) {
      var cellValue = world.active.get(x, y);
      var i = y * worldWidth * 4 + x * 4; // 4 is bytes per color
      pixels[i + 0] = cellValue ? 0 : 255;
      pixels[i + 1] = cellValue ? 0 : 255;
      pixels[i + 2] = cellValue ? 0 : 255;
      pixels[i + 3] = 255; // indexing goes up to bytes per color - 1
    }
  }
  ctx.putImageData(imgData, 0, 0);
  setTimeout(render, 0); // this seems to be a bit faster than request animation frame, even tho somewhat less "correct"
  stats.render = timers.render();

  ctx.font = "bold 25px sans-serif";
  ctx.fillStyle = "#FF0000";
  ctx.fillText(
    `${worldWidth}x${worldHeight}=${worldWidth * worldHeight} cells`,
    10,
    30
  );
  ctx.fillText(`update: ${stats.update | 0} ms`, 10, 60);
  ctx.fillText(`render: ${stats.render | 0} ms`, 10, 90);
  ctx.fillText(`threads: ${WORKERS_TOTAL}`, 10, 110);
}
